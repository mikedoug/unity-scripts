﻿using UnityEngine;
using System.Collections;

public class TieRotation : MonoBehaviour {
	public Transform other;

	// Use this for initialization
	void Start () {
		Update ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.localRotation = other.localRotation;
	}
}
