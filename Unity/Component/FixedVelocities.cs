﻿/*
 * FixedVelocities: v1.0
 * 
 * A Unity Component Script to set fixed velocities on an object
 * 
 */
 
using UnityEngine;
using System.Collections;

/// <summary>
/// Given the specified fixed velocities, apply them to the rigid body on Start.
/// 
/// NOTE: If you don't want the item to moving/rotating, make sure you remove
/// the drag and/or angular drag.
/// </summary>
public class FixedVelocities : MonoBehaviour {

	public Vector3 velocity = Vector3.zero;
	public Vector3 angularVelocity = Vector3.zero;
	
	void Start() {
		rigidbody.velocity = velocity;
		rigidbody.angularVelocity = angularVelocity;
	}
}
