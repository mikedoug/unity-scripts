﻿/*
 * RandomForce: v1.0
 * 
 * A Unity Component Script to apply a random force to an element
 * 
 */
 
using UnityEngine;
using System.Collections;

/// <summary>
/// Random force applies a force with each component randomized between a min and
/// max value.  The force randomizes every ChangeInterval seconds.
/// </summary>
public class RandomForce : MonoBehaviour {
	void FixedUpdate () {
		if (Time.time > nextTime) {
			force.x = (MinX == 0 && MaxX == 0) ? 0 : Random.value * (MaxX - MinX) + MinX;
			force.y = (MinY == 0 && MaxY == 0) ? 0 : Random.value * (MaxY - MinY) + MinY;
			force.z = (MinZ == 0 && MaxZ == 0) ? 0 : Random.value * (MaxZ - MinZ) + MinZ;
			nextTime = Time.time + ChangeInterval;
		}
		rigidbody.AddForce(force);
	}
	
	private float nextTime = 0;
	private Vector3 force;
	
	public float ChangeInterval = 1;
	public float MinX = 0;
	public float MaxX = 0;
	public float MinY = 0;
	public float MaxY = 0;
	public float MinZ = 0;
	public float MaxZ = 0;
}
