﻿/*
 * DropObjectGravity: v1.0
 * 
 * A Unity Script to use gravity to place an object on the ground naturally.
 * 
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*
 * Drop Object with Gravity Usage:
 *
 * 0. Be in Edit mode (NOT Play mode)
 * 1. Select ONE object (currently only works with the first selected object).
 * 2. Click "Drop with Gravity"
 * 3. Wait while the editor goes into play mode and drops your object to the
 *	ground.  Changes will be automatically applied once the object stabilizes.
 * 4. If the object doesn't stabilize, you can exit edit mode or click cancel
 *	to terminate the attempt.
 *
 * This won't work on all things -- but simple mesh objects it should work nicely
 * on.  I tested this with some rocks from the Unity store.
 *
 */
 


public class DropObjectGravity : EditorWindow {

	/// <summary>
	/// This object handles all of the work of dropping a single game object
	/// </summary>
	[System.Serializable]
	private class GameObjectDropper {
		private GameObject original;
		private GameObject worker;
		
		private bool enabled;
		private Vector3 position = Vector3.zero;
		private Quaternion rotation = Quaternion.identity;	
		private int matchCount = 0;
		
		public GameObject gameObject { get { return worker; } }
		public bool done { get { return matchCount > 20; } }
		
		public GameObjectDropper(GameObject Original) {
			original = Original;
			
			// Create our worker object, and position it properly
			worker = (GameObject) Instantiate(original);
			worker.transform.position = original.transform.position;
			worker.transform.rotation = original.transform.rotation;
			
			// Disable the original object so it doesn't get in the way of gravity
			enabled = original.activeSelf;
			original.SetActive(false);
	
			// Get our MeshCollider
			MeshCollider col = worker.GetComponent<MeshCollider>();
			if (col == null) {
				// If there's a different collider, let's get rid of it
				if (worker.collider != null) {
					UnityEngine.Object.DestroyImmediate(worker.collider);
				}
	
				// Add our MeshCollider
				worker.AddComponent ("MeshCollider");
				col = worker.GetComponent<MeshCollider>();
			}
	
			// Make sure we're convex, and enabled
			col.convex = true;
			col.enabled = true;
	
			// Make this a rigidbody so it will fall
			if (worker.rigidbody == null) {
				worker.AddComponent("Rigidbody");
			}
			worker.rigidbody.useGravity = true;			
		}

		public void apply() {
			// Apply our position/rotation to the original
			if (original != null) {
				original.transform.position = position;
				original.transform.rotation = rotation;
			}
		}
		
		public void cleanup() {
			// Reset our original Game Object's enabled value
			original.SetActive(enabled);
			original = null;
	
			// Destroy our temporary GameObject
			DestroyImmediate(worker);
			worker = null;
		}
		
		/// <summary>
		/// Update this instance.  Return true if it's done moving; false if not
		/// </summary>
		public bool update() {
			if (position == worker.transform.position && rotation == worker.transform.rotation) {
				if (done) {
					return true; // true == done!
				} else {
					matchCount += 1;
				}
			} else {
				position = worker.transform.position;
				rotation = worker.transform.rotation;
				matchCount = 0;
				/*
				if (position.y < -1000) {
					Debug.Log ("Failure: Object has fallen below -1000");
					state = State.Failure;
					EditorApplication.isPlaying = false;
				}
				*/
			}
			
			return false; // false == not done!
		}		
	}
	
	private enum State { Idle, Starting, Running, Success, Failure, Cancel };
	
	private List<GameObjectDropper> droppers = null;

	private GameObject myCamera = null;
	private bool cameraMoved = false;
		
	private State state = State.Idle;
	
	// Use this for initialization
	[MenuItem( "Edit/Drop Item with Gravity %_g" )]
	static void Init () {
		DropObjectGravity window = (DropObjectGravity)EditorWindow.GetWindow( typeof(DropObjectGravity) );
		window.maxSize = new Vector2( 300, 200 );
	}

	public DropObjectGravity() {
		EditorApplication.playmodeStateChanged += onPlaymodeStateChange;
	}
	
	public void OnEnable() {
		// There's a deserialization issue that a null object pointer deserializes an "all defaults" object.
		// OnEnable is called after deserialization -- so let's clear invalid droppers here.
		if (droppers != null && droppers.Capacity == 0)
			droppers = null;
	}

	public void OnGUI()
	{
		EditorGUILayout.LabelField("Use the Gravity!", EditorStyles.boldLabel);
		if (state == State.Idle) {
			if (GUILayout.Button("Drop with Gravity")) {
				StartIt();
			}
		} else {
			if (GUILayout.Button("Cancel")) {
				ButtonCancel();
			}
		}

		// Debug Items
		/*
		EditorGUILayout.EnumPopup("State", state);
		EditorGUILayout.Toggle("Has droppers", droppers != null);
		if (droppers != null) {
			EditorGUILayout.Toggle("Has 0 droppers", droppers == null || droppers.Capacity == 0);
		}
		*/
	}
	
	/// <summary>
	/// Preps the camera move - this is done in onPlayStateChange false before the true
	/// </summary>
	private void PrepMoveCamera() {
		SceneView sv = EditorWindow.GetWindow<SceneView>();
				
		if (sv == null) {
			Debug.Log ("SceneView is null; not moving the camera");
			return;
		}
		
		myCamera = new GameObject("Drop with Gravity Camera");
		Camera cam = (Camera) myCamera.AddComponent("Camera");
		cam.transform.position = sv.camera.transform.position;
		cam.transform.rotation = sv.camera.transform.rotation;
	}
	
	/// <summary>
	/// Completes moving the camera -- this is done onPlayStateChange true after the false
	/// </summary>
	private void MoveCamera() {
		Object[] cameras = FindObjectsOfType(typeof(Camera));
		
		// Set all other camera's to inactive state
		foreach(Camera camera in cameras) {
			if (camera.gameObject != myCamera) {
				camera.gameObject.SetActive(false);
			}
		}
		
		cameraMoved = true;
	}
	
	private void ReturnCamera() {
		if (myCamera != null) {
			DestroyImmediate(myCamera);
			myCamera = null;
		}
		
		cameraMoved = false;
		// The active states for the "other cameras" in the scene will revert back when we go from play -> edit mode.
	}

	private void StartIt() {
		if (EditorApplication.isPlaying) {
			Debug.Log ("Editor already in play mode...  Cannot start!");
			return;
		}
		
		// Record our original Game Object
		List<Object> undoObjects = new List<Object>();
		foreach(GameObject gameObject in Selection.gameObjects) {
			Object[] objects = EditorUtility.CollectDeepHierarchy(new Object[]{gameObject});
			foreach(Object o in objects) {
				undoObjects.Add (o);
			}
		}
		
		Undo.RegisterUndo(undoObjects.ToArray(), "Drop with Gravity");
		
		droppers = new List<GameObjectDropper>();
		foreach(GameObject gameObject in Selection.gameObjects) {
			droppers.Add(new GameObjectDropper(gameObject));
		}

		// We use State.Starting because setting isPlaying to true fires an erroneous playmodeStateChanged event while isPlaying is still false..
		state = State.Starting;
		EditorApplication.isPlaying = true;
		state = State.Running;
	}

	public void onPlaymodeStateChange() {
		if (EditorApplication.isPlaying == false) {
			if (state == State.Starting) {
				PrepMoveCamera();
			} else if (state == State.Running) {
				Debug.Log ("Play Mode terminated before object settled; cancelling.");
				CleanUp();
			} else if (state == State.Success) {
				// Apply changes!
				if (droppers != null) {
					foreach(GameObjectDropper dropper in droppers) {
						dropper.apply();
					}
				}
				CleanUp();
			} else if (state == State.Cancel || state == State.Failure) {
				Debug.Log (state + " detected.");
				CleanUp ();
			}
		} else {
			// We move the camera PURELY in play mode
			if (state == State.Running && cameraMoved == false) {
				MoveCamera ();
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (EditorApplication.isPlaying && state == State.Running && droppers != null) {

			// If any object is still moving, don't do anything.
			foreach(GameObjectDropper dropper in droppers) {
				if (!dropper.update()) {
					myCamera.transform.LookAt(dropper.gameObject.transform);
					return;
				}
			}
			
			// All of the objects are done!
			state = State.Success;
			EditorApplication.isPlaying = false;
		}
	}

	void ButtonCancel() {
		state = State.Cancel;
		EditorApplication.isPlaying = false;
	}

	void CleanUp() {
		if (droppers != null) {
			foreach(GameObjectDropper dropper in droppers) {
				dropper.cleanup();
			}
			droppers = null;
		}
		state = State.Idle;

		ReturnCamera();
		
		// Force the GUI to redraw our buttons
		Repaint();
	}
}
