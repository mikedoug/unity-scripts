﻿using UnityEditor;
using System.Collections.Generic;
using System.Linq;

// Customer Editor to differentiate fields for Master and Non-Master
namespace GameSystem {
	[CustomEditor (typeof(InventoryController))]
	[CanEditMultipleObjects]
	public class InventoryControllerEditor : Editor {
		public override void OnInspectorGUI() {
			InventoryController obj = (InventoryController)target;
			
			obj.IsMaster = EditorGUILayout.Toggle("Is Master", obj.IsMaster);
			if (obj.IsMaster) {
				if (obj.gameObject.GetComponents<InventoryController>().Where((ic) => {return ic.IsMaster;}).Count() > 1) {
					EditorGUILayout.LabelField(" **** ERROR - MULTIPLE MASTERS ****");
				}
				obj.AutoWieldIntoSlots = EditorGUILayout.Toggle("Auto Wield Into Slots", obj.AutoWieldIntoSlots);
			} else {
				obj.SlotName = EditorGUILayout.TextField("Slot Name", obj.SlotName);	
				obj.AcceptsType = (InventoryItem.Type)EditorGUILayout.EnumPopup("Accepts Type", obj.AcceptsType);		
			}
			
			EditorGUIUtility.LookLikeInspector();
			SerializedProperty items = serializedObject.FindProperty ("items");
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(items, true);
			if(EditorGUI.EndChangeCheck()) {
				serializedObject.ApplyModifiedProperties();
			}
			EditorGUIUtility.LookLikeControls();
			
			obj.maxSize = EditorGUILayout.IntField("Max Items", obj.maxSize);
			obj.holder = (UnityEngine.Transform)EditorGUILayout.ObjectField("Holder", obj.holder, typeof(UnityEngine.Transform), true);
		}
	}
}