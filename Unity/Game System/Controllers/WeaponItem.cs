﻿using UnityEngine;
using System.Collections;


namespace GameSystem {
	/// <summary>
	/// Top level category for items which can be used as weapons
	/// </summary> 
	public class WeaponItem : InventoryItem {
		public float damage = 0;
	}
}