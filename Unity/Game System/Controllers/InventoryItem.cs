﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Inventory Items are any items which can be placed into an InventoryController
/// </summary>

namespace GameSystem {
	/// <summary>
	/// Any items which the player can pick up and (possibly) wield.
	/// </summary>
	public class InventoryItem : MonoBehaviour {
		public enum Type {
			All, Weapon, Helmet
		}
		
		/// <summary>
		/// The type.
		/// </summary>
		public Type type;
		/// <summary>
		/// New local position when wielded (relative to Player Controller)
		/// </summary>
		public bool OnWieldUpdatePosition = false;
		public Vector3 wieldPosition = Vector3.zero;
		private Vector3 oldPosition = Vector3.zero;
		
		/// <summary>
		/// New local rotation when wielded (relative to Player Controller)
		/// </summary>
		public bool OnWieldUpdateRotation = false;
		public Vector3 wieldRotation = Vector3.zero;
		private Vector3 oldRotation = Vector3.zero;

				/// <summary>
		/// New local scale when wielded (relative to Player Controller)
		/// </summary>
		public bool OnWieldUpdateScale = false;
		public Vector3 wieldScale = Vector3.one;
		private Vector3 oldScale = Vector3.zero;
		
		public InventoryController InventoryController = null;
		
		// Dropped true makes this NO pickup on collision (so we can drop an item and not immediately pick it back up)
		private bool dropped = false;
		public bool Dropped { get {return dropped;} set { dropped = value; } }
		
		public void OnMouseDown() {
			// This is done rarely, so we'll take teh hit to find the PlayerController object
			PlayerController pc = (PlayerController)GameObject.FindObjectOfType(typeof(PlayerController));
			pc.PickupItem(this, PlayerController.PickupType.Click);
		}
		
		public void OnWield() {
			if (OnWieldUpdatePosition) {
				oldPosition = transform.localPosition;
				transform.localPosition = wieldPosition;
			}
			if (OnWieldUpdateRotation) {
				oldRotation = transform.localEulerAngles;
				transform.localEulerAngles = wieldRotation;
			}
			if (OnWieldUpdateScale) {
				oldScale = transform.localScale;
				transform.localScale = wieldScale;
			}
		}
		
		public void OnUnWield() {
			print ("OnUnWield: " + this.gameObject.name);
			if (OnWieldUpdatePosition) {
				transform.localPosition = oldPosition;
			}
			if (OnWieldUpdateRotation) {
				transform.localEulerAngles = oldRotation;
			}
			if (OnWieldUpdateScale) {
				transform.localScale = oldScale;
			}
		}
	} 
}