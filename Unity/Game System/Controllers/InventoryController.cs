﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace GameSystem {

	public class InventoryController : MonoBehaviour {
		public bool IsMaster = false;
		public string SlotName;
		public bool AutoWieldIntoSlots = true;
		public InventoryItem.Type AcceptsType;
		// A list of our InventoryItem objects
		public List<InventoryItem> items = new List<InventoryItem>();
		private Dictionary<string, InventoryController> slotDictionary = new Dictionary<string, InventoryController>();
		
		public Dictionary<string, InventoryController> SlotDictionary { get { return slotDictionary; } }
		public int Count { get { return items.Count; } }

		// holder is where we re-parent any contained items
		public Transform holder;
		public int maxSize = 20;

		public void Start() {
			if (holder == null) {
				holder = new GameObject("Inventoried Items " + SlotName).transform;
				holder.parent = this.transform;
				holder.gameObject.SetActive(false);
			}
			
			// An empty SlotName means this is the master inventory for this object; build slot dictionary here
			if (string.IsNullOrEmpty(SlotName)) {
				foreach (InventoryController ic in this.gameObject.GetComponents<InventoryController>()) {
					if (ic != this) {
						slotDictionary.Add (ic.SlotName, ic);
					}
				}
			}
		}
		
		public InventoryController getSlot(string name) {
			if (slotDictionary.ContainsKey(name)) {
				return slotDictionary[name];
			}
			
			return null;
		}
		
		public bool checkItem(InventoryItem item) {
			return AcceptsType == InventoryItem.Type.All || AcceptsType == item.type;
		}
		
		/// <summary>
		/// Adds the given item into the inventory.  If AutoWieldIntoSlots is enabled, then we will look
		/// for an empty slot matching the item to automatically assign it to.
		/// </summary>
		/// <returns>
		/// The true if the item was accepted, false otherwise
		/// </returns>
		/// <param name='item'>
		/// The item to add.
		/// </param> 
		public bool addItem(InventoryItem item) {
			return addItem (item, false);
		}
		
		// This private method has a force parameter.  If true, we will not perform auto-wield
		// logic and we will bypass the maxSize check.
		private bool addItem(InventoryItem item, bool force) {
			InventoryController slot = findSlotForItem(item);
			
			// autoWield is only done if we're not FORCING an action, and there's an empty slot available
			bool autoWield = AutoWieldIntoSlots && slot != null && slot.Count == 0 && slot.checkItem(item);
			
			// Size check: onely if force=false and autoWield = false; when autoWield = true the item will
			// immediately move into a slot.
			if (!force && !autoWield && items.Count >= maxSize) {
				return false;
			}
			
			Debug.Log ("Added " + item + " to inventory");
			
			// If the item is in another InventoryController, remove it from there first
			if (item.InventoryController != null) {
				item.InventoryController.removeItem(item);
			}
			
			// Add it to our list
			items.Add(item);
			item.InventoryController = this;
			item.gameObject.transform.parent = holder;
			
			// Wield the item if we determined we will auto wield this; don't autoWield when force=True
			if (!force && autoWield) {
				Wield (item);
			}
			
			return true;
		}
		
		/// <summary>
		/// Swaps the items between their two respective inventory controllers.
		/// 
		/// This bypasses size checking in addItem so if some crazy scripts reduces
		/// the size of an InventoryController below its current count, the items
		/// still move without losing one of the objects.
		/// 
		/// This also bypasses autoWielding in addItem since swapitems is intended to
		/// always work.
		/// 
		/// Requires both items to be IN an InventoryController.  Will throw NPE if not.
		/// </summary>
		public void swapItems(InventoryItem item1, InventoryItem item2) {
			InventoryController ic1 = item1.InventoryController;
			InventoryController ic2 = item2.InventoryController;
			
			// Adding an item removes it from its previous controller; also we use force=true
			// for the bypassing effects described above
			ic1.addItem(item2, true);
			ic2.addItem(item1, true);
		}
		
		/// <summary>
		/// Removes the item from the controller.  Note it does NOT reparent -- the
		/// caller will be responsible for parenting this object someplace sane.
		/// </summary>
		public bool removeItem(InventoryItem item) {
			if (items.Contains(item)) {
				Debug.Log ("Removed " + item + " from inventory");
				
				// We're a wield slot if we're not the master InventoryController
				if (!IsMaster) {
					item.OnUnWield();
				}
				
				item.InventoryController = null;
				items.Remove(item);
				return true;
			}
			
			return false;
		}
		
		public bool containsItem(InventoryItem item) {
			return items.Contains (item);
		}
		
		public InventoryItem getItemAt(int index) {
			if (index < items.Count) {
				return items[index];
			}
			return null;
		}
		
		/// <summary>
		/// Finds the first InventoryItem matching the specified type.
		/// </summary>
		/// <returns>
		/// The InventoryItem object.
		/// </returns>
		/// <typeparam name='T'>
		/// The type of object to search the inventory for.
		/// </typeparam>
		public InventoryItem getItem<T>() {
			foreach (InventoryItem ii in items) {
				if (ii is T) {
					return ii;
				}
			}
			
			return null;
		}
		
		// Requires item to be IN the inventory bag
		public bool Wield(InventoryItem item) {
			if (! items.Contains(item)) {
				print ("Item not in inventory, cannot wield it: " + item.name);
				return false;
			}
			
			// Find a slot accepting of this item
			InventoryController slot = findSlotForItem(item);
			if (slot == null) {
				print ("Item does not fit into any available slot: " + item.name);
				return false;
			}
			
			print ("wielding into slot: " + slot.SlotName);
			
			if (slot.Count == 0) {
				print ("adding; slot count is " + slot.Count);
				slot.addItem(item);
			} else {
				// Hrm.. This has an odd requirement that both items must be in an InventoryController already...
				// which is handled by requiring that the item being wielded already be in the inventory...
				print ("swapping");
				slot.swapItems(item, slot.getItemAt(0)); // moronically swaps with the first slot
			}

			// These are special changes we make for wielded items...
			item.collider.enabled = false; // Disable the collider
			item.OnWield();

			// This should preserve the old values so when it is dropped...
			
			return true;
		}
		
		// Bug: We can overfill our inventory bag by unwielding...
		public bool Unwield(InventoryItem item) {
			InventoryController slot = findSlotForItem(item);
			if (!slot.containsItem(item)) {
				print ("Item " + item.name + " not in slot " + slot.SlotName);
				return false;
			}
			
			// Add the item to ourselves, force=true to make it not auto-wield :)
			item.OnUnWield();
			return addItem(item, true);
		}
		
		private InventoryController findSlotForItem(InventoryItem item) {
			foreach (InventoryController slot in SlotDictionary.Values) {
				if (slot.checkItem(item)) {
					return slot;
				}
			}
			return null;
		}
		
		public bool IsWielded(InventoryItem item) {
			InventoryController ic = findSlotForItem(item);
			return ic != null && ic.containsItem(item);
		}

	}
}