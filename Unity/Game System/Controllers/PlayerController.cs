﻿using UnityEngine;
using System.Collections;

namespace GameSystem {
	
	/// <summary>
	/// This is the Player Controller which handles running into triggers in the environment.
	/// </summary>
	public class PlayerController : MonoBehaviour {
		// The GameObject holding our InventoryController component.
		public InventoryController inventoryController = null;
		
		// Which methods of pickup do we support
		public bool PickupByTrigger = true;
		public bool PickupByClick = true;
		
		public void Start() {
			// Add our PlayerInventoryView
			PlayerInventoryView playerInventoryView = gameObject.AddComponent<PlayerInventoryView>();
			playerInventoryView.inventoryController = inventoryController;
		}
		
		// Handle pickup/wield of items we trigger
		public void OnTriggerEnter(Collider other) {
			InventoryItem item = other.gameObject.GetComponent<InventoryItem>();
			
			// If the item has an InventoryItem component, and is not flagged as Dropped; pick it up
			if (item != null && !item.Dropped) {
				PickupItem(item, PickupType.Trigger);
			}
		}
		
		public void OnTriggerExit(Collider other) {
			InventoryItem item = other.gameObject.GetComponent<InventoryItem>();
			
			// If the item has an InventoryItem component, and Dropped is true; reset Dropped
			if (item != null && item.Dropped) {
				item.Dropped = false;
			}
		}
		
		public enum PickupType { Trigger, Click };
		public void PickupItem(InventoryItem item, PickupType pickupType) {
			// Only allow permitted pickup methods
			if ((pickupType == PickupType.Click && !PickupByClick) ||
				(pickupType == PickupType.Trigger && !PickupByTrigger)) {
				return;
			}
			
			if (inventoryController.addItem(item)) {
				print ("Picked up " + item.ToString ());
			} else { 
				print ("Inventory Full; Can't pickup " + item.ToString ());
			}
		}
	}
}