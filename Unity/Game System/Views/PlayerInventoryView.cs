﻿using UnityEngine;
using System.Collections;

namespace GameSystem {
	public class PlayerInventoryView : MonoBehaviour {
		public InventoryController inventoryController;
		public bool showInventory = true;
		
	    public Rect windowRect = new Rect(20, 20, 140, Screen.height - 40);
	    void OnGUI() {
			if (showInventory) {
	        	windowRect = GUI.Window(0, windowRect, DoMyWindow, "My Window");
			}
	    }
	    void DoMyWindow(int windowID) {
			int y = 25;
			
			// Show all of our top-level slots (doesn't do sub-slots)
			foreach (InventoryController ic in inventoryController.SlotDictionary.Values) {
				if (ic.items.Count > 0) {
					GUI.Label (new Rect(20, y, 120, 20), ic.SlotName + ":");
					y += 20;
					
					ic.items.ForEach((i) => y = DoItemButton(y, i));
				}
			}
			
			GUI.Label(new Rect(20, y, 120, 20), "In Inventory:");
			y += 20;
			
			foreach (InventoryItem item in inventoryController.items.ToArray()) {
				y = DoItemButton (y, item);
			}
	    }
		
		private int DoItemButton(int y, InventoryItem item) {
        	if (GUI.Button(new Rect(10, y, 120, 15), item.name)) {
				if (Event.current.button == 0) {
					// Left mouse button: Wield/Unwield
					print ("Left: " + item.name);
					
					if (inventoryController.IsWielded(item)) {
						inventoryController.Unwield(item);
					} else {
						inventoryController.Wield(item);
					}
				} else if (Event.current.button == 1) {
					// Right mouse button: Drop
					
					item.InventoryController.removeItem(item);
					
					item.Dropped = true;
					item.collider.enabled = true;
					item.transform.parent = transform.parent;
					item.transform.localEulerAngles = Vector3.zero;
					item.transform.localPosition = transform.localPosition;
				}
			}
			return y + 20;
		}
		
		public void Update() {
			// Toggle our inventory on the "Inventory" button
			if (Input.GetButtonDown("Inventory")) {
				showInventory = !showInventory;
			}
		}
	}
}