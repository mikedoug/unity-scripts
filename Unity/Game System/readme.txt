Game System is a basic set of objects where I'm trying to solve a few problems:

 1. Pickup and carry objects from the scene in an "inventory bag".
 2. Wield items into different types of "slots".
 3. Manage the inventory bag and wielded item.

Basic Usage:
 
  1. Add an empty GameObject to your Player Controller, rename it
     InventoryBag (or somesuch) and add an InventoryController (IC)
     component to it.  This is the master or parent IC and MUST be
     the first IC on the InventoryBag game object.  This initial IC
     should have the following properties:
     
     a. Slot Name: Leave this blank.  A blank slot name indicates
        that this is the master or parent IC.
        
     b. Auto Wield Into Slots: True if you want auto assignment of
        items picked up into empty slots.
        
     c. Accepts Type: This is ignored on the master IC.
     
     d. Holder: Leave none -- the script will auto create a holder
        game object automatically for the master IC.
        
     e. Max Size: How many items do you want to be able to carry?

  2. Add the PlayerController component on your main Player Controller
     object (e.g. on your First Person Controller).  Assign the
     following properties of the component:

     a. Inventory Container: Drag the InventoryBag object onto this
	    slot.  This component is used as the bag to place all
	    inventory items.

     b. Pickup By Trigger: Set to true if colliding with InventoryItem
        objects should pick them up.
        
     c. Pickup By Click: Set to true if you clicking on an InventoryItem
        with the mouse cursor should pick them up.
        
  3. Add to the InventoryBag any number of additional InventoryController
     modules with the following properties:

     a. Slot Name: Give a good, descriptive name of this slot.  Examples
        include: hand, head, foot.
        
     b. Auto Wield Into Slots: This value is ignored for slots.
        
     c. Accepts Type: The type of object this slot holds.  Currently two
        slots should NOT have the same type -- only the first one would
        be used.
     
     d. Holder: You can leave this set to none and an automatically
        created HIDDEN holder will be created for you.  BUT, if you want
        the items to move visible when their or "wielded", the give a
        game object to which the object will be placed when added to the
        slot.  For example: An empty game slot at the top of the head
        could be used for a "head" slot so hats/helmets appear placed
        properly upon the head.
        
     e. Max Size: How many items can this slot hold.  Typically 1, but
        can be more.

  4. Any object which can be picked up should have the InventoryItem
     component added to it.  To support picking up objects when the
     player collides with them, set the collider on the object to
     'trigger'.  In order to be wielded, set the type to match the
     slot it should be assigned to; if it's not wieldable leave the
     type set to All.
     
  4. To use the "Player Inventory" window, you must define an "Inventory"
     axes under Edit -> Project Settings -> Input.  I defined this as the
     key 'i' in my projects.

Inventory Management:

  You can bring up the player inventory by pressing the "Inventory"
  button (define a button in "Inputs").
  
  The window will show what item is wielded and what items are in
  the inventory.  Left-Clicking will unwield a wielded item and
  wield an unwielded item.  Right-clicking will drop an item.

Item Pickup Methods:

  A GameObject with a collider marked as a trigger which also
  contains the InventoryItem component will be "picked up" and added
  to the inventory when the player triggers the item.
  
  Clicking on a GameObject will pick that item up too.  There is no
  limitation on distance with this method currently.

Item Wielding Method:

  Currently, if you walk over a wieldable item, and you have not
  previously wielded something, it is wielded instead of being added
  to the inventory.  It also has its local position, rotation, and
  scale set (so you can see it in-camera).
  
  You can also wield an item in the player inventory by left-clicking
  an item in the inventory.

FUTURE ENHANCEMENTS:
   * GUI to view an inventory bag.
   * Possibly support InventoryItem as a child of the actual GameObject
     picked up (in case the collider of the actual object is needed for
     gravity work).
   * Sample scene to make this work.
   * Support for 3rd person wielding
